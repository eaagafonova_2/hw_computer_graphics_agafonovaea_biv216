# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 23:57:52 2022

@author: user
"""

import cv2
import subprocess
print("Подождите, видео перекодируется...")
subprocess.call(['ffmpeg', '-i', 'task1_2.mp4', '-vf', 'scale=960x540', 'task1.mp4'])
# cv2.VideoCapture(0); # видео поток с веб камеры
cap = cv2.VideoCapture("task1.mp4"); 
fps = int(cap.get(5))
print("Частота кадров: ", fps, "кадров в секунду")  
# Получить количество кадров
frame_count = int(cap.get(7))
print("Количество кадров: ", frame_count)
file_count = 0
i = 0
j = 0
motion = []
alltime = []
ret, frame1 = cap.read()
ret, frame2 = cap.read()
while cap.isOpened(): # метод isOpened() выводит статус видеопотока
  diff = cv2.absdiff(frame1, frame2)# нахождение разницы двух кадров, которая проявляется лишь при изменении одного из них, т.е. с этого момента наша программа реагирует на любое движение.

  file_count += 1
  
  gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY) # перевод кадров в черно-белую градацию
  
  blur = cv2.GaussianBlur(gray, (5, 5), 0) # фильтрация лишних контуров
 
  _, thresh = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY) # метод для выделения кромки объекта белым цветом
 
  dilated = cv2.dilate(thresh, None, iterations = 3) # данный метод противоположен методу erosion(), т.е. эрозии объекта, и расширяет выделенную на предыдущем этапе область
 
  сontours, _ = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # нахождение массива контурных точек
  
  for contour in сontours:
    (x, y, w, h) = cv2.boundingRect(contour)# преобразование массива из предыдущего этапа в кортеж из четырех координат
    
    if cv2.contourArea(contour) < 700: # условие при котором площадь выделенного объекта меньше 700 px
        continue
    cv2.rectangle(frame1, (x, y), (x+w, y+h), (120, 45, 105), 2) # получение прямоугольника из точек кортеж
    
    N = file_count / 60
    m1 = N // 60
    m = int(m1 % 60)
    s = int(N % 60)
    if N>=86400:
        h=int(m1//60-24*(m1//60//24))
    else:
        h = int(m1 // 60)
        if m > 9:
            if  s > 9:
                #print(h, "0:", m, ":", s, sep="")
                time = str(h) + "0:" + str(m) + ":" + str(s)
                
            else:
                #print(h, "0:", m, ":0", s, sep="")
                time = str(h) + "0:" + str(m) + ":0" + str(s) 
        else:
            if  s > 9:
                #print(h, "0:0", m, ":", s, sep="")
                time = str(h) + "0:0" + str(m) + ":" + str(s) 
                
            else:
                #print(h, "0:0", m, ":0", s, sep="")
                time = str(h) + "0:0" + str(m) + ":0" + str(s)
    if len(alltime) == 0:
        alltime.append(time)
        motion.append(alltime[0])
    else:
        if alltime[-1] != time:
            alltime.append(time)
            
    if i+1 < len(alltime):
        sec2 = int(alltime[i+1].split(":")[2]) + int(alltime[i+1].split(":")[1])*60 + int(alltime[i+1].split(":")[0])*3600
        sec1 = int(alltime[i].split(":")[2])+int(alltime[i].split(":")[1])*60 + int(alltime[i].split(":")[0])*3600
        if sec2 - sec1 > 1:
            motion.append(alltime[i])
            motion.append(alltime[i+1])
        i += 1
    # if cv2.contourArea(contour) < 700: # условие при котором площадь выделенного объекта меньше 700 px
    #     continue
    # cv2.rectangle(frame1, (x, y), (x+w, y+h), (0, 255, 0), 2) # получение прямоугольника из точек кортеж
  cv2.imshow("frame1", frame1)
  frame1 = frame2  #
  ret, frame2 = cap.read() #
  
  if ret == 0:
      break
 
  if cv2.waitKey(1) == 27:
    break
cap.release()
cv2.destroyAllWindows()
motion.append(alltime[-1])
print("Список всего времени появления человека в кадре:")
print(alltime)
print("Список времени начала и конца появления человека в кадре:")
print(motion)
k = 1
start = []
end = []
print("Подождите, идет создание видео с человеком и запись их названий в файл...")
while j < len(motion):
    if j % 2 == 0:
        start.append(motion[j])
    else:
        end.append(motion[j])
    if len(start)==len(end) and start[-1]!=end[-1]:
        filename = "out"+str(k)+".mp4"
        subprocess.call(['ffmpeg', '-ss',start[-1] , '-to', end[-1], '-i', 'task1.mp4', filename])
        filename = "'out{col}.mp4'".format(col=k)
        with open('names.txt', 'a') as file:
            file.write("file ")
            file.write('{word}\n'.format(word=filename))
        k+=1
    j+=1
print("Подождите, идет склейка всех видео...")
subprocess.call(['ffmpeg', '-f', 'concat','-i', 'names.txt', 'output.mp4'])  
open('names.txt', 'w').close()
print("Программа выполнена успешно")